#source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

#autoload -Uz compinit promptinit
#compinit
#promptinit
#export GOPATH=$HOME/project/gocode
source /etc/profile
plugins=(git github perl)
ZSH_THEME="powerline"
ZSH=/usr/share/oh-my-zsh
ZSH_CUSTOM=$ZSH
source $ZSH/oh-my-zsh.sh

PATH=$PATH:$GOPATH/bin:/opt/amin/hugo_0.17_linux_amd64
# This will set the default prompt to the walters theme
#prompt bigfade
#ZSH_THEME="powerline"
prompt grml-large
